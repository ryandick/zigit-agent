module.exports = function generateUUID( substringCount ) {
	var d = new Date().getTime();
	var uuid = 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace( /[xy]/g, function ( c ) {
		var r = ( d + Math.random() * 16 ) % 16 | 0;
		d = Math.floor( d / 16 );
		return ( c == 'x' ? r : r & 7 | 8 ).toString( 16 );
	} );
	if ( !substringCount ) return uuid;
	else return uuid.substring( 0, substringCount );
};